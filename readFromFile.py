#readFromFile created by João Gubert
#31/08/18

import fileinput
import csv
import math
import tree

# -------- Funções Auxiliares --------

def printData(matriz):
    for i in range(len(matriz)):
        s = 'Instancia ' + repr(i) + ': '
        print(s)
        for j in range(len(matriz[0])):
            print(matriz[i][j])
        print()

def printAtributo(matriz, num):
    for i in range(14):
        s = 'Instancia ' + repr(i) + ': '
        print(s)
        print(matriz[i][num])
        print()

def info(matriz, lines, collumns):
    yes_counter = 0
    no_counter = 0
    for i in range(1, lines+1):
        if (matriz[i][collumns] == "Sim"):
            yes_counter += 1
        elif (matriz[i][collumns] == "Nao"):
            no_counter += 1
    if(yes_counter == 0 or no_counter == 0):
        return 1
    info_gain = -(yes_counter/lines)*math.log2(yes_counter/lines) -(no_counter/lines)*math.log2(no_counter/lines)
    return info_gain

def infoGain(matriz, atributo, lines, collumns):
    yes_counter = []
    no_counter = []
    atrib = []
    for i in range(1, lines+1):
        if (atrib.count(matriz[i][atributo]) == 0):
            atrib.append(matriz[i][atributo])
            yes_counter.append(0)
            no_counter.append(0)
        index = atrib.index(matriz[i][atributo])
        if (matriz[i][collumns] == "Sim"):
            yes_counter[index] += 1
        elif (matriz[i][collumns] == "Nao"):
            no_counter[index] += 1

    info_gain_atrib = 0

    for i in range(len(atrib)):
        occurrences = yes_counter[i] + no_counter[i]
        if (no_counter[i] == 0):
            continue
        if (yes_counter[i] == 0):
            continue
        info_gain_atrib = info_gain_atrib+(occurrences/lines)*(-(yes_counter[i]/occurrences)*math.log2(yes_counter[i]/occurrences)-(no_counter[i]/occurrences)*math.log2(no_counter[i]/occurrences))

    info_gain_total = info(matriz, lines, collumns)
    info_gain_atrib = info_gain_total - info_gain_atrib
    return info_gain_atrib

# ----------------------------------
def makeTree(matriz, node):
    yes_counter = 0
    no_counter = 0
    for i in range(len(matriz)):
        if(matriz[i][len(matriz[0])-1] == "Sim"):
            yes_counter += 1
        elif (matriz[i][len(matriz[0])-1] == "Nao"):
            no_counter += 1
    if(yes_counter == 0 and no_counter != 0):
        node.setValue("Nao")
        return
    if(no_counter == 0 and yes_counter != 0):
        node.setValue("Sim")
        return
    #ID3
    attrib_chosen = 0
    enthalpy_chosen = 0
    for i in range(len(matriz[0])-1):
        attrib_gain = infoGain(matriz, i, len(matriz)-1, len(matriz[0])-1)
        if (enthalpy_chosen < attrib_gain):
            attrib_chosen = i
            enthalpy_chosen = attrib_gain
    s = "Atributo: " + str(attrib_chosen) + " Entalpia: " + str(enthalpy_chosen)
    node.setValue(matriz[0][attrib_chosen])
    #popula filhos
    sons = []
    for i in range(1, len(matriz)):
        if (sons.count(matriz[i][attrib_chosen]) == 0):
            sons.append(matriz[i][attrib_chosen])
    #chama função de forma recursiva para os filhos
    for son_key in sons:
        new_data = list(matriz)
        son_node = tree.Node()
        node.addSon(son_node, son_key)
        #exclui linhas que não tem son_key de new_data
        aux_remove = []
        for i in range(1, len(new_data)):
            if(new_data[i][attrib_chosen] != son_key):
                aux_remove.append(i)
        for i in reversed(aux_remove):
            new_data.remove(new_data[i])

        makeTree(new_data, son_node)


#-----------------------------------
data = []
d = {

}
for line in fileinput.input():
    input = fileinput.filename()

with open(input) as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=';')
    line_count = 0
    for row in csv_reader:
        data.append([0]*len(row))
        if line_count == 0:
            #print(f'{" ".join(row)}')
            for i in range(len(row)-1):
                d[i] = row[i]
                data[line_count][i] = row[i]
            line_count += 1
        else:
            for i in range(len(row)):
                #print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
                data[line_count][i] = row[i]
            #print(f'{" ".join(row)}')
            line_count += 1
    print(f'Processed {line_count} lines.')
#printData(data)
#printAtributo(data, 2)
#print(info(data))
#print(infoGain(data,0))

#Descobrir atributo com maior ganho de informação

root = tree.Node()

makeTree(data, root)
tree.printTree(root, 0, "")
