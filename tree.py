
class Node:
    def __init__(self):
        self.value = None
        self.sons_index = {}
        self.sons = []

    def setValue(self, value):
        self.value = value

    def addSon(self, Node, key):
        self.sons_index[len(self.sons)] = key
        self.sons.append(Node)

    def __repr__(self):
        return '%s' % (self.value)

def printTree(Node, level, key):
    s = ""
    for i in range(level):
        s = s + "\t"
    s = s + key + " -> "
    if (level == 0):
        s = ""
    print(s + str(Node.value))
    if (len(Node.sons) != 0):
        for j in range(len(Node.sons)):
            Node.sons_index[j]
            printTree(Node.sons[j], level + 1, Node.sons_index[j])
